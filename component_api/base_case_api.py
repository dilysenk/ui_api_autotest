from json import JSONDecodeError

from requests import Response
from assertpy import assert_that


class BaseCase:

    def get_cookie(self, response: Response, cookie_name):
        assert_that(response.cookies, f'отсутствует cookie {cookie_name}').contains(cookie_name)
        return response.cookies[cookie_name]

    def get_header(self, response: Response, headers_name):
        assert_that(response.cookies, f'отсутствует cookie {headers_name}').contains(headers_name)
        return response.headers[headers_name]

    def get_json(self, response: Response, name):
        try:
            response_as_dict = response.json()
        except JSONDecodeError:
            raise AssertionError(f'в запросе отсутствует json {response.text}')
        assert_that(response_as_dict, f'отсутствует cookie {name}').contains(name)
        return response_as_dict[name]

    def get_status(self, response):
        assert_that(response.status_code, f'status is {response.status_code}').is_not_equal_to(500)
