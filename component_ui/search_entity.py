from playwright.sync_api import Page

import allure


class SearchBy:

    def __init__(self, browser: Page, display_name, container,
                 input_selector=None,
                 container_menu=None,
                 entity_in_menu=None):
        self.browser = browser
        self.display_name = display_name
        self.container = container  # контейнер с полем
        self.input_selector = input_selector
        self.container_menu = container_menu  # контейнер с вариантами
        self.entity_in_menu = entity_in_menu

    def set_value(self, value):
        if value is None:
            return
        self._name()
        self._entity_in_menu()
        with allure.step(f'заполним поле {self._name()} значением {value}'):
            self.browser.fill(self._input_field(), value)
        self.browser.is_visible(self._container_menu())
        self.browser.locator(self._container_menu(), has_text=value)
        return value

    def _name(self):
        if self.display_name is None:
            return 'Неизвестное поле (Имя не указано)'

    def _entity_in_menu(self):
        if self.entity_in_menu is None:
            return ''
        else:
            return self.entity_in_menu

    def _input_field(self):
        if self.input_selector is None:
            return self.container + " " + 'input'
        else:
            return self.container + " " + self.input_selector

    def _container_menu(self):
        if self.container_menu is None:
            return
        return self.container_menu

    def get_value(self):
        return self.browser.get_attribute(self.container, 'text')
