from playwright.async_api import Page

from component_ui.fill_model import FillModel


class Form(FillModel):

    def __init__(self, browser: Page):
        self.browser = browser
        self.add_button = None
        self.save = None

    def open_add_form(self):
        self.browser.click(self.add_button)
        return self

    def press_save(self):
        self.browser.click(self.save)

    def create(self, model):
        self.open_add_form() \
            .fill_in_fields(model)
        self.save()

    def edit(self, element, model):
        self.browser.click(element)
        self.fill_in_fields(model)
        self.save()
