
from playwright.async_api import Page

import allure


class DropDownMenu:

    def __init__(
            self,
            browser: Page,
            display_name,
            container,
            name=None,
            container_with_entity=None,
            entity_in_menu=None
    ):
        self.browser = browser
        self.display_name = display_name
        self.name = name  # Название поля
        self.container = container  # контейнер с полем
        self.container_with_entity = container_with_entity  # контейнер с вариантами
        self.entity_in_menu = entity_in_menu  # варианты в меню

    def set_value(self, value):
        if value is None:
            return
        with allure.step(f'кликнем по раскрывающемся меню {self._name}'):
            self.browser.click(self.container)
        self.browser.is_visible(self._container_with_entity())
        self.browser.locator(self.entity_in_menu, has_text=value).click()

    def _name(self):
        if self.name is None:
            return 'Неизвестное поле (Имя не указано)'
        else:
            return self.name

    def _container_with_entity(self):
        if self.container_with_entity is None:
            return self.container
        else:
            return self.container_with_entity

    def get_value(self):
        return self.browser.get_attribute(self.container, 'text')
