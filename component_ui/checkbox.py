import allure
from playwright.sync_api import Page


class CheckBox:

    def __init__(self, browser: Page, display_name, container):
        self.browser = browser
        self.container = container
        self.display_name = display_name

    def set_value(self, value):
        if value is None:
            return
        with allure.step(f'чек-бокс {self.display_name}'):
            self.browser.click(self.container)

