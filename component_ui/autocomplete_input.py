from playwright.async_api import Page

import allure


class AutoCompleteInput:

    def __init__(self, browser: Page, display_name, container,
                 name=None,
                 input_selector=None,
                 container_menu=None,
                 entity_in_menu=None):
        self.browser = browser
        self.display_name = display_name
        self.container = container  # контейнер с полем
        self.container_menu = container_menu  # контейнер с вариантами
        self.name = name  # Название поля
        self.entity_in_menu = entity_in_menu
        self.input_selector = input_selector

    #
    def set_value(self, value):
        if value is None:
            return
        with allure.step(f'заполним поле {self._name()} значением {value}'):
            self.browser.fill(self._input_field(), value)
        self.browser.is_visible(self._container_menu())
        self.browser.locator(self.entity_in_menu, has_text=value).click()
        return value

    def _name(self):
        if self.name is None:
            return 'Неизвестное поле (Имя не указано)'

    def _input_field(self):
        if self.input_selector is None:
            return self.container + " " + 'input'
        else:
            return self.container + " " + self.input_selector

    def _container_menu(self):
        if self.container_menu is None:
            return self.container

    def get_value(self):
        return self.browser.get_attribute(self.container, 'text')
