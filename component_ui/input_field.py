import allure
from playwright.sync_api import Page


class InputField:

    def __init__(self, browser: Page, display_name, container, input_selector=None):
        self.browser = browser
        self.display_name = display_name
        self.container = container
        self.input_selector = input_selector

    def set_value(self, value):
        if value is None:
            return
        with allure.step(f"{self.display_name} == '{value}'"):
            self.browser.is_visible(self.container)
            self.browser.fill(self.container,  str(value))
            return value

    def get_value(self):
        return self.browser.get_attribute(self.container, 'text')
