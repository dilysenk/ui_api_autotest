import allure


class BasePage:

    def __init__(self, browser):
        self.browser = browser
        self.url = "localhost"

    def navigate(self):
        with allure.step(f'Открываем экран {self.url}'):
            self.browser.goto(self.url)
            return self
