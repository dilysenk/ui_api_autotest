class FillModel:
    def fill_in_fields(self, model_input=None):
        if model_input is None:
            return self
        dataclass_fields = [field for field in model_input.__dataclass_fields__]
        for field in dataclass_fields:
            element = getattr(self, f"{field}")
            try:
                element.set_value(getattr(model_input, field))
            except AttributeError:
                raise AssertionError(f'Невозможно заполнить поле {field}')
        return self

