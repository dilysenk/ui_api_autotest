FROM mcr.microsoft.com/playwright/python:v1.30.0-jammy

WORKDIR /app

COPY requirements.txt .

RUN pip install -U pip
RUN pip install -r requirements.txt

COPY . .

CMD pytest -v tests