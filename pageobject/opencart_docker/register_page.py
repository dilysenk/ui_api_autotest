from dataclasses import dataclass

import allure
from playwright.sync_api import Page

from component_ui.base_page import BasePage

from component_ui.fill_model import FillModel
from component_ui.input_field import InputField
from config import settings as cfg


class CssRegisterPage:
    FIRST_NAME = '[placeholder="First Name"]'
    LAST_NAME = '[placeholder="Last Name"]'
    EMAIL = '[placeholder="E-Mail"]'
    TELEPHONE = '[placeholder="Telephone"]'
    PASSWORD = '[placeholder="Password"]'
    PASSWORD_CONFIRM = '[placeholder="Password Confirm"]'
    CHECK_BOX_AGREE = '[name="agree"]'
    CONTINUE = '[value="Continue"]'


@dataclass
class RegisterModel:
    first_name: str = None
    last_name: str = None
    email: str = None
    telephone: str = None
    password: str = None
    password_confirm: str = None


class RegisterForm(FillModel):

    def __init__(self, browser):
        self.first_name = InputField(browser, 'Name', CssRegisterPage.FIRST_NAME)
        self.last_name = InputField(browser, 'Last_name', CssRegisterPage.LAST_NAME)
        self.email = InputField(browser, 'Email', CssRegisterPage.EMAIL)
        self.telephone = InputField(browser, 'Telephone', CssRegisterPage.TELEPHONE)
        self.password = InputField(browser, 'Password', CssRegisterPage.PASSWORD)
        self.password_confirm = InputField(browser, 'Password_confirm', CssRegisterPage.PASSWORD_CONFIRM)


class RegisterPage(BasePage):

    def __init__(self, browser: Page):
        super().__init__(browser)
        self.browser = browser
        self.url = f'http://{cfg.url.ip_docker}:7070/index.php?route=account/register'
        self.register_form = RegisterForm(self.browser)

    def agree_policy(self):
        with allure.step('click in registration'):
            self.browser.click(CssRegisterPage.CHECK_BOX_AGREE)
            return self

    def click_continue(self):
        with allure.step('click in continue'):
            self.browser.click(CssRegisterPage.CONTINUE)

    def navigate(self):
        with allure.step(f'Открываем экран {self.url}'):
            self.browser.goto(self.url)
            return self
