import allure
from playwright.async_api import Page

from component_ui.base_page import BasePage
from config import settings as cfg


class CssMainPage:
    CURRENCY = '.btn.btn-link.dropdown-toggle'
    CABINET = '.hidden-xs.hidden-sm.hidden-md'


class MainPage(BasePage):
    POUND_STERLING = "£ Pound Sterling"
    TABLETS = ''

    def __init__(self, browser: Page):
        super().__init__(browser)
        self.browser = browser
        self.url = f'http://{cfg.url.ip_docker}:7070'

    def change_currency(self):
        with allure.step('Изменить валюту'):
            self.browser.click(CssMainPage.CURRENCY)
            self.browser.get_by_text(self.POUND_STERLING).click()

    def forward_to_register(self):
        with allure.step('Перейти к регистрации'):
            self.browser.click(CssMainPage.CABINET)
            self.browser.get_by_text('Регистрация').click()

    def choose_sort_by(self, value):
        with allure.step(f'выбрать сортировку {value}'):
            self.browser.get_by_text('Tablets').click()
