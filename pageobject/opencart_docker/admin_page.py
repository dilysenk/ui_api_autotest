from dataclasses import dataclass

from playwright.sync_api import Page

from component_ui.base_page import BasePage
from component_ui.form import Form
from config import settings as cfg

import allure

from component_ui.input_field import InputField


class CssAdminPage:
    CATALOG = '[data-toggle="collapse"]'
    PRODUCTS = '.collapse.in li + li a'
    ADD_NEW = '[data-original-title="Add New"]'
    PRODUCT_NAME = '[placeholder="Product Name"]'
    META_TAG_TITLE = '[placeholder="Meta Tag Title"]'
    MODEL = '[placeholder="Model"]'
    SAVE = '.fa.fa-save'
    SELECT_PRODUCT = '[type="checkbox"]'
    DELETE = '.fa.fa-trash-o'
    TABLE = '.table.table-bordered.table-hover'
    SUCCESS_DELETE = '.alert.alert-success.alert-dismissible'
    EDIT = '[data-original-title="Edit"]'


@dataclass
class AdminProductModel:
    product_name: str = None
    meta_tag_title: str = None
    model: str = None


class AddProductForm(Form):
    def __init__(self, browser):
        super().__init__(browser)
        self.product_name = InputField(browser, 'Product Name', CssAdminPage.PRODUCT_NAME)
        self.meta_tag_title = InputField(browser, 'Meta Tag Title', CssAdminPage.META_TAG_TITLE)
        self.model = InputField(browser, 'Meta Tag Title', CssAdminPage.MODEL)
        self.add_button = CssAdminPage.ADD_NEW
        self.save = CssAdminPage.SAVE


class AdminProduct(BasePage):

    def __init__(self, browser: Page):
        super().__init__(browser)
        self.browser = browser
        self.url = f'http://{cfg.url.ip_docker}:7070/admin/'
        self.add_product = AddProductForm(self.browser)

    def go_to_products(self):
        with allure.step('перейти в раздел продукты'):
            try:
                self.browser.click(CssAdminPage.PRODUCTS)
            except:
                self.browser.click(CssAdminPage.CATALOG)
                self.browser.click(CssAdminPage.PRODUCTS)
            return AddProductForm(self.browser)

    def select_product(self):
        with allure.step('выбор продукта'):
            self.browser.click(CssAdminPage.SELECT_PRODUCT)
            return self

    def delete_product(self):
        with allure.step('удалить продукт'):
            self.browser.click(CssAdminPage.DELETE)
            # def handle_dialog(dialog):
            #     assert dialog.type == 'beforeunload'
            #     dialog.accept()
            #
            # self.browser.on('dialog', lambda: handle_dialog)
            # self.browser.close(run_before_unload=True)
            #
            # alert = self.browser
            # alert.accept()
            return self
