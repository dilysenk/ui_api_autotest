from dataclasses import dataclass


from component_ui.form import Form
from component_ui.input_field import InputField
from config import settings as cfg
import allure


class CssLoginAdminPage:
    USERNAME_ADMIN = '[name="username"]'
    PASSWORD_ADMIN = '[name="password"]'
    LOGIN_BUTTON_ADMIN = ".btn.btn-primary"


@dataclass
class LoginPageModel:
    user_name: str = None
    password: str = None


class LoginPageForm(Form):

    def __init__(self, browser):
        super().__init__(browser)
        self.browser = browser
        self.user_name = InputField(browser, 'UserName', CssLoginAdminPage.USERNAME_ADMIN)
        self.password = InputField(browser, 'Password', CssLoginAdminPage.PASSWORD_ADMIN)
        self.url = f'http://{cfg.url.ip_docker}:7070/admin/'

    def navigate(self):
        with allure.step(f'Открываем экран {self.url}'):
            self.browser.goto(self.url)
            return self

    def to_login(self):
        self.fill_in_fields(LoginPageModel(user_name=cfg.user.username,
                                           password=cfg.user.password))
        self.browser.click(CssLoginAdminPage.LOGIN_BUTTON_ADMIN)
