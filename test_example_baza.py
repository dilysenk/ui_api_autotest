from dataclasses import dataclass
from enum import Enum
from functools import partial

import pytest


@dataclass()
class ModelMovie:
    pass

@pytest.fixture()
def movie():
    return 'taxi'


@pytest.fixture()
def year():
    return '1999'


@pytest.fixture()
def season():
    return '2'


class Movie(Enum):
    model = ModelMovie()
    # переход на страницу создания
    navigate = 'navigate_to_create_form'
    name = 'movie'
    year = 'year'
    season = 'season'
    # expected = 'proverki'


@pytest.fixture()
def model(request):
    def _inner(arguments):
        model = ModelMovie()

        for fixture in arguments:
            key: str = fixture.name
            value: str = request.getfixturevalue(fixture.value)
            setattr(model, key, value)
        return model

    return _inner


@pytest.mark.parametrize('models',
                         [
                             (Movie.name, Movie.year),
                             (Movie.name, Movie.year, Movie.season)
                         ])
def test_create(model, models, movie, year):
    init_model = model(models)
    assert init_model.name == movie
    assert init_model.year == year


@pytest.fixture()
def model_indirect(request):
    model = ModelMovie()
    for fixture in request.param:
        key: str = fixture.name
        value: str = request.getfixturevalue(fixture.value)
        setattr(model, key, value)
    return model


@pytest.mark.parametrize('model_indirect',
                         [
                             (Movie.name, Movie.year),
                             (Movie.name, Movie.year, Movie.season)
                         ], indirect=True)
def test_create_indirect(model_indirect, movie, year):
    """
    версия с индиректом

    """
    assert model_indirect.name == movie
    assert model_indirect.year == year


def _inject(cls, names):
    @pytest.fixture(autouse=True)
    def _auto_injector_fixture(self, request):
        for name in names:
            setattr(self, name, request.getfixturevalue(name))

    cls.__auto_injector_fixture = _auto_injector_fixture
    return cls


def auto_inject_fixtures(*names):
    return partial(_inject, names=names)


# class TestShmest:
#
#     def __init__(self, name):
#         self.name = name
#
#     @pytest.mark.parametrize('model', [self.name])
#     def test_shmest(self, model):
#         pass

all_storage = pytest.mark.parametrize(
    "a, b, c",
    [
        pytest.param(
            3, 5, 8,
            id="sum 3 + 5 = 8",
        ),
        pytest.param(
            1, 2, 3,
            id="sum 1 + 2 = 3",
        ),
    ])


@all_storage
def test_sum(a, b, c):
    assert a + b == c
