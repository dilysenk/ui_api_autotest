import pytest
import logging
import allure
from faker import Faker
from playwright.sync_api import Page

from component_api.my_requests import MyRequests
from helper_func import create_dir_logs

create_dir_logs()

with open("tests/test_api/end_points", 'r') as params:
    list_params = params.readlines()
    list_endpoints = [i.strip('\n') for i in list_params]


@pytest.fixture(scope='function', autouse=True)
def log_fixture(request, browser):
    logger = logging.getLogger('BrowserLogger')
    test_name = request.node.name

    logger.info(f"===> Test started name, test is {test_name}")

    def fin():
        logger.info(f"===> Test finished name, test is {test_name}")

    request.addfinalizer(fin)


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)


@pytest.fixture(scope="function", autouse=True)
def screen_failure(request, pw: Page):
    def fin():
        try:
            request.node.rep_call.failed
        except AttributeError:
            raise AssertionError("Тест не стартовал, Ошибка")
        if request.node.rep_call.failed:
            allure.attach(
                pw.screenshot(),
                name="finalizer attach",
                attachment_type=allure.attachment_type.PNG,
            )

    request.addfinalizer(fin)


@pytest.fixture(name="end_point", params=list_endpoints)
def get_end_point(request):
    return request.param


@pytest.fixture
def response_get(end_point):
    return MyRequests.request_get(end_point)


@pytest.fixture
def faker():
    return Faker()


@pytest.fixture(scope="session")
def pw(playwright):
    browser = playwright.chromium.launch(headless=True)
    page = browser.new_page()
    return page
