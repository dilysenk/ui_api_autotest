import pytest
import allure

from tests.tests_opencart_for_docker.base import BasePageCase
from pageobject.opencart_docker.admin_page import AdminProductModel, CssAdminPage
from pageobject.opencart_docker.main_page import MainPage


class TestOpenCart(BasePageCase):

    def test_chrome(self, pw):
        pw.goto('https://ya.ru/')
        pw.get_by_text('найдется всё')
        
    @allure.description("""Проверка создания пользователя""")
    @allure.title('тест создание пользоватиеля')
    @pytest.mark.flaky(reruns=3)
    def test_registration(self, browser, faker, get_model_register_model):
        model = get_model_register_model(
            'first_name',
            'last_name',
            'email',
            "telephone",
            "password",
            'password_confirm'
        )
        self.registered_page.navigate()
        self.registered_page.register_form. \
            fill_in_fields(model)
        self.registered_page. \
            agree_policy(). \
            click_continue()
        # assert "success" in self.registered_page.browser.browser.current_url, 'Ожидалась ссылка c success'

    @allure.title('Добавление нового товара')
    @allure.description("""Добавление нового контента в список товаров""")
    def test_add_new_item(self, faker):
        name = faker.color()
        self.login_page.navigate().to_login()
        self.admin_page.go_to_products()
        add_product_from = self.admin_page.add_product.open_add_form()
        add_product_from.fill_in_fields(AdminProductModel(product_name=name, meta_tag_title=name))
        add_product_from.browser.get_by_text('Data').click()
        add_product_from.fill_in_fields(AdminProductModel(model=name))
        add_product_from.press_save()
        assert self.admin_page.browser.get_by_text(name), f"не найден товар{name}"

    @allure.title('Редактирование товара')
    @allure.description("Редактирование товара")
    def test_update_item(self, browser, create_new_item, faker):
        name = faker.color()
        self.admin_page.go_to_products()
        self.admin_page.browser.click(CssAdminPage.EDIT)
        self.admin_page.add_product.fill_in_fields(AdminProductModel(product_name=name, meta_tag_title=name))
        self.admin_page.browser.get_by_text('Data').click()
        self.admin_page.add_product.fill_in_fields(AdminProductModel(model=name))
        self.admin_page.browser.click(CssAdminPage.SAVE)
        assert self.admin_page.browser.get_by_text(name), f"не найден товар{name}"

    @allure.description("Проверка удаление товара с первой строчки")
    @allure.title('тест удаление товара')
    def test_delete_item(self, create_new_item):
        self.admin_page.go_to_products()
        self.admin_page \
            .select_product() \
            .delete_product()
        assert self.admin_page.browser.is_visible(CssAdminPage.SUCCESS_DELETE), "товар не удалён"

    @allure.description("Проверка изменение валюты на главной странице")
    @allure.title('тест изменение валюты')
    def test_switch_currency(self, pw):
        MainPage(pw). \
            navigate(). \
            change_currency()
        assert MainPage(pw).browser.get_by_text('£'), "валюта не переведена"

    @allure.description("Проверка изменение валюты на главной странице")
    @allure.title('тест изменение валюты ')
    @pytest.mark.xfail
    def test_switch_currency_fail(self, pw):
        p = MainPage(pw)
        p. \
            navigate(). \
            change_currency()
        assert p.browser.get_by_text('₽'), "не должно быть такой валюты"
