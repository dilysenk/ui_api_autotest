import pytest
from _pytest.fixtures import FixtureRequest

from pageobject.opencart_docker.admin_page import AdminProduct
from pageobject.opencart_docker.login_page import LoginPageForm
from pageobject.opencart_docker.register_page import RegisterPage


class BasePageCase:

    @pytest.fixture(scope='function', autouse=True)
    def setup(self, request: FixtureRequest):
        self.registered_page: RegisterPage = request.getfixturevalue('registered_page')
        self.admin_page: AdminProduct = request.getfixturevalue('admin_page')
        self.login_page: LoginPageForm = request.getfixturevalue('login_page')
