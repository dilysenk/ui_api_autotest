import pytest
from _pytest.fixtures import FixtureRequest

from pageobject.opencart_docker.admin_page import AdminProduct, CssAdminPage, AdminProductModel
from pageobject.opencart_docker.login_page import LoginPageForm
from pageobject.opencart_docker.register_page import RegisterPage, RegisterModel





@pytest.fixture(scope="session")
def registered_page(pw):
    return RegisterPage(pw)


@pytest.fixture(scope="session")
def admin_page(pw):
    return AdminProduct(pw)


@pytest.fixture(scope="session")
def login_page(pw):
    return LoginPageForm(pw)


@pytest.fixture
def create_new_item(browser, admin_page, login_page, faker):
    login_page.navigate().to_login()
    add_product_form = admin_page.go_to_products()
    add_product_form.browser.click(CssAdminPage.ADD_NEW)
    admin_page.add_product.fill_in_fields(AdminProductModel(product_name=faker.color(), meta_tag_title=faker.color()))
    admin_page.browser.get_by_text('Data').click()
    admin_page.add_product.fill_in_fields(AdminProductModel(model=faker.color()))
    admin_page.browser.click(CssAdminPage.SAVE)
    return faker.color()


class BaseCase:

    def __init__(self):
        self.login_page = None
        self.admin_page = None
        self.registered_page = None

    @pytest.fixture(scope='function', autouse=True)
    def setup(self, request: FixtureRequest):
        self.registered_page: RegisterPage = request.getfixturevalue('registered_page')
        self.admin_page: AdminProduct = request.getfixturevalue('admin_page')
        self.login_page: LoginPageForm = request.getfixturevalue('login_page')


@pytest.fixture(scope="function")
def get_model_register_model(request):
    """ Формирование RegisterModel с пробросом аргументов """

    def _inner(*args):
        model = RegisterModel()
        for key in args:
            value = None
            match key:
                case 'first_name' | 'last_name':
                    value = request.getfixturevalue('faker').name()
                case 'email':
                    value = request.getfixturevalue('faker').email()
                case "telephone":
                    value = request.getfixturevalue('faker').phone_number()
                case "password" | 'password_confirm':
                    value = 2099
            if value is None:
                raise AssertionError(f'Ключ {value} равен None')
            try:
                setattr(model, key, value)
            except TypeError as e:
                raise AssertionError(f'{model} = {value} = {e}')
        return model

    return _inner
